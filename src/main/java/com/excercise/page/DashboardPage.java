package com.excercise.page;

import org.openqa.selenium.WebDriver;

import com.excercise.page.component.AbstractComponent;
import com.excercise.page.component.MenuBar;
import com.excercise.page.component.PopupMessage;

public class DashboardPage extends AbstractComponent<DashboardPage> {
	
	protected MenuBar menuBar;
	protected PopupMessage popupMessage;
	
	public MenuBar getMenuBar() {
		return menuBar;
	}
	
	public PopupMessage getPopupMessage() {
		return popupMessage;
	}

	public DashboardPage(WebDriver driver) {
		super(driver);
		menuBar = new MenuBar(driver);
		popupMessage = new PopupMessage(driver);
	}

	@Override
	protected DashboardPage getThis() {
		return this;
	}
}
