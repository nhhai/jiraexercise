package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PopupMessage extends AbstractComponent<PopupMessage> {
	
	private By divPopupMessageBy = By.cssSelector(".aui-message");
	
	public PopupMessage(WebDriver driver) {
		super(driver);
	}
	
	public String getMessageAtIndex(int index) {
		WebElement element = driver.findElements(divPopupMessageBy).get(index);
		return element.getText();
	}

	@Override
	protected PopupMessage getThis() {
		return this;
	}
}
