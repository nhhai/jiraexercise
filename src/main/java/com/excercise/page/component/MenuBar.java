package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.excercise.page.LogInPage;

public class MenuBar extends AbstractComponent<MenuBar> {
	
	private By linkCreateIssueBy = By.id("create_link");
	private By linkSignInBy = By.linkText("Log In");
	private By textboxQuickSearchBy = By.id("quickSearchInput");
	
	public MenuBar(WebDriver driver) {
		super(driver);
	}
	
	public CreateIssueForm clickCreate() {
		driver.findElement(linkCreateIssueBy).click();
		return new CreateIssueForm(driver);
	}
	
	public LogInPage clickLogIn() {
		driver.findElement(linkSignInBy).click();
		return new LogInPage(driver);
	}

	public <T> T quickSearchByText(String text, Class<T> returnPageClass) {
		WebElement ele = driver.findElement(textboxQuickSearchBy);
		ele.clear();
		ele.sendKeys(text + Keys.ENTER);
		try {
			return returnPageClass.getConstructor(WebDriver.class).newInstance(this.driver);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	protected MenuBar getThis() {
		return this;
	}
}
