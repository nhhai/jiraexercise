package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UpdateIssueForm extends EditIssueForm<UpdateIssueForm> {

	private By buttonUpdateBy = By.id("edit-issue-submit");
	
	public UpdateIssueForm(WebDriver driver) {
		super(driver);
	}
	
	public <T> T clickUpdate(Class<T> returnPageClass) {
		driver.findElement(buttonUpdateBy).click();
		try {
			return returnPageClass.getConstructor(WebDriver.class).newInstance(this.driver);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected UpdateIssueForm getThis() {
		return this;
	}

}
