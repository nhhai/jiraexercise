package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class EditIssueForm<T extends EditIssueForm<T>> extends AbstractComponent<T> {

	private By textboxProjectBy = By.id("project-field");
	private By textboxIssueTypeBy = By.id("issuetype-field");
	private By textboxSummaryBy = By.id("summary");
	private By textboxAssigneeBy = By.id("assignee-field");
	private By spanThrobberBy = By.cssSelector("#create-issue-dialog .buttons .throbber");

	public EditIssueForm(WebDriver driver) {
		super(driver);
	}

	public T setProject(String projectName) {
		waitUntilInputEnabled(textboxProjectBy);
		WebElement ele = driver.findElement(textboxProjectBy);
		ele.clear();
		ele.sendKeys(projectName + Keys.TAB);
		waitUntilFinishAjaxLoading();
		return getThis();
	}

	public T setIssueType(String issueType) {
		waitUntilInputEnabled(textboxIssueTypeBy);
		WebElement ele = driver.findElement(textboxIssueTypeBy);
		ele.clear();
		ele.sendKeys(issueType + Keys.TAB);
		waitUntilFinishAjaxLoading();
		return getThis();
	}

	public T setSummary(String summary) {
		waitUntilInputEnabled(textboxSummaryBy);
		WebElement ele = driver.findElement(textboxSummaryBy);
		ele.clear();
		ele.sendKeys(summary);
		return getThis();
	}

	public T setAssignee(String assignee) {
		waitUntilInputEnabled(textboxAssigneeBy);
		WebElement ele = driver.findElement(textboxAssigneeBy);
		ele.clear();
		ele.sendKeys(assignee);
		waitUntilFinishAjaxLoading();
		driver.findElement(By.xpath(String.format("//a[em[text()='%s']]", assignee))).click();
		return getThis();
	}

	protected void waitUntilFinishAjaxLoading() {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				WebElement spanThrobber = driver.findElement(spanThrobberBy);
				String classAttribute = spanThrobber.getAttribute("class");
				return !classAttribute.contains("loading");
			}
		});
	}

	protected void waitUntilInputEnabled(final By elementBy) {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				WebElement element = driver.findElement(elementBy);
				return element.isEnabled();
			}
		});
	}

}
