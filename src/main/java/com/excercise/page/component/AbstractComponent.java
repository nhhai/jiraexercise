package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractComponent<T extends AbstractComponent<T>> {

	protected WebDriver driver;
	
	public AbstractComponent(WebDriver driver) {
		this.driver = driver;
	}
	
	public T delay(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return getThis();
	}
	
	public String getPageTitle() {
		return driver.getTitle();
	}
	
	protected T waitUntilElementDisappeared(By elementBy, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(elementBy));
		return getThis();
	}
	
	protected T waitUntilElementAppeared(By elementBy, int timeOut) {
		WebDriverWait wait = new WebDriverWait(driver, timeOut);
		wait.until(ExpectedConditions.presenceOfElementLocated(elementBy));
		return getThis();
	}
	
	protected abstract T getThis();
}
