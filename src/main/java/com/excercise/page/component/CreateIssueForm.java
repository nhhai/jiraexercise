package com.excercise.page.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateIssueForm extends EditIssueForm<CreateIssueForm> {

	private By checkboxCreateAnotherBy = By.id("qf-create-another");
	private By buttonCreateBy = By.id("create-issue-submit");

	public CreateIssueForm(WebDriver driver) {
		super(driver);
	}

	public CreateIssueForm createAndContinue() {
		WebElement checkboxCreateAnother = driver.findElement(checkboxCreateAnotherBy);
		if (!checkboxCreateAnother.isSelected()) {
			checkboxCreateAnother.click();
		}

		driver.findElement(buttonCreateBy).click();
		waitUntilElementDisappeared(buttonCreateBy, 90);
		return this;
	}

	public void createAndClose() {
		WebElement checkboxCreateAnother = driver.findElement(checkboxCreateAnotherBy);
		if (checkboxCreateAnother.isSelected()) {
			checkboxCreateAnother.click();
		}

		driver.findElement(buttonCreateBy).click();
	}

	@Override
	protected CreateIssueForm getThis() {
		return this;
	}

}
