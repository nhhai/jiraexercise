package com.excercise.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.excercise.page.component.AbstractComponent;

public class LogInPage extends AbstractComponent<LogInPage> {
	
	private By textboxEmailBy = By.id("username");
	private By textboxPasswordBy = By.id("password");
	private By buttonLogInBy = By.id("login-submit");
	
	public LogInPage(WebDriver driver) {
		super(driver);
	}
	
	public LogInPage setEmail(String email) {
		driver.findElement(textboxEmailBy).sendKeys(email);
		return this;
	}
	
	public LogInPage setPassword(String password) {
		driver.findElement(textboxPasswordBy).sendKeys(password);
		return this;
	}
	
	public <T> T clickLogIn(Class<T> returnPageClass) {
		driver.findElement(buttonLogInBy).click();
		try {
			return returnPageClass.getConstructor(WebDriver.class).newInstance(this.driver);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected LogInPage getThis() {
		return this;
	}
}
