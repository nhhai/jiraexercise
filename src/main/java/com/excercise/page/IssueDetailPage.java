package com.excercise.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.excercise.page.component.AbstractComponent;
import com.excercise.page.component.UpdateIssueForm;

public class IssueDetailPage extends AbstractComponent<IssueDetailPage> {

	private By buttonEditBy = By.id("edit-issue");
	private By lableSummary = By.id("summary-val");
	private By lableKey = By.id("key-val");
	private By lableAssignee = By.id("assignee-val");
	private By lableType = By.id("type-val");
	
	public IssueDetailPage(WebDriver driver) {
		super(driver);
	}
	
	public UpdateIssueForm clickEdit() {
		driver.findElement(buttonEditBy).click();
		return new UpdateIssueForm(driver);
	}
	
	public String getIssueSummary() {
		return driver.findElement(lableSummary).getText();
	}
	
	public String getIssueKey() {
		return driver.findElement(lableKey).getText();
	}
	
	public String getIssueType() {
		return driver.findElement(lableType).getText().replaceAll("[\\t\\n\\r]+"," ").trim();
	}
	
	public String getIssueAssignee() {
		return driver.findElement(lableAssignee).getText().replaceAll("[\\t\\n\\r]+"," ").trim();
	}

	@Override
	protected IssueDetailPage getThis() {
		return this;
	}

}
