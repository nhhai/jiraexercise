package com.excercise.aut;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class WebAutomationTest {

	protected WebDriver driver;
		
	@BeforeMethod
	public void beforeMethod(ITestContext ctx) {
		String hubUrl = ctx.getCurrentXmlTest().getParameter("hubUrl");
		String browser = ctx.getCurrentXmlTest().getParameter("browser");
		String version = ctx.getCurrentXmlTest().getParameter("version");
		String platform = ctx.getCurrentXmlTest().getParameter("platform");
		
		try {
			this.driver = (hubUrl == null || hubUrl.isEmpty() ?
					WebDriverFactory.createWebDriver(browser) :
					WebDriverFactory.createWebDriver(browser, version, platform, hubUrl));
			
			driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} catch (MalformedURLException e) {
			System.out.print("[ERROR] Error while launching browser");
			e.printStackTrace();
		}
	}
	
	@AfterMethod
	public void afterMethod() {
		driver.quit();
	}

}
