package com.excercise.aut;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverFactory {
	
	public static final int DEFAULT_TIMEOUT = 5;

	public static final String BROWSER_FIREFOX = "firefox";
	public static final String BROWSER_CHROME = "chrome";
	public static final String BROWSER_SAFARI = "safari";
	public static final String BROWSER_IE = "internet explorer";

	public static final String PLATFORM_MAC = "mac";
	public static final String PLATFORM_WINDOWS = "windows";
	public static final String PLATFORM_LINUX = "linux";
	
	static {
		// add chrome driver and ie driver to system path
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("test.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String os = System.getProperty("os.name").toLowerCase();
		if(os.contains(PLATFORM_LINUX)) {
			System.setProperty("webdriver.chrome.driver", props.getProperty("webdriver.chrome.driver.linux"));
		}
		else if(os.contains(PLATFORM_MAC)) {
			System.setProperty("webdriver.chrome.driver", props.getProperty("webdriver.chrome.driver.mac"));
		}
		else if(os.contains(PLATFORM_WINDOWS)) {
			System.setProperty("webdriver.chrome.driver", props.getProperty("webdriver.chrome.driver.win"));
		}
		
		System.setProperty("webdriver.ie.driver", props.getProperty("webdriver.ie.driver"));
		System.setProperty("webdriver.firefox.bin", props.getProperty("webdriver.firefox.bin"));
	}

	private static DesiredCapabilities createDesiredCapabilities(String browser, String version, String platform) {

		// setup browser
		DesiredCapabilities dc = new DesiredCapabilities();

		if (browser == null || browser.isEmpty() || browser.contains(BROWSER_CHROME))
			dc.setBrowserName(DesiredCapabilities.chrome().getBrowserName());
		else if (browser.contains(BROWSER_FIREFOX) || browser.equals("ff"))
			dc.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
		else if (browser.contains(BROWSER_SAFARI))
			dc.setBrowserName(DesiredCapabilities.safari().getBrowserName());
		else if (browser.contains(BROWSER_IE) || browser.equals("ie"))
			dc.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());
		else
			dc.setBrowserName(DesiredCapabilities.internetExplorer().getBrowserName());

		// setup version
		if (version != null && !version.isEmpty() && !version.equals("any"))
			dc.setVersion(version);

		// setup platform
		if (platform == null || platform.isEmpty() || platform.equals("any"))
			dc.setPlatform(Platform.ANY);
		else if (platform.contains(PLATFORM_MAC))
			dc.setPlatform(Platform.MAC);
		else if (platform.contains(PLATFORM_WINDOWS))
			dc.setPlatform(Platform.WINDOWS);
		else if (platform.contains(PLATFORM_LINUX))
			dc.setPlatform(Platform.LINUX);
		else
			dc.setPlatform(Platform.ANY);

		return dc;
	}

	public static WebDriver createWebDriver(String browser) {

		WebDriver driver = null;
		browser = browser == null ? null : browser.toLowerCase();
		
		// setup driver
		if (browser == null || browser.isEmpty() || browser.contains(BROWSER_CHROME)) {
			driver = new ChromeDriver();
		}
		else if (browser.contains(BROWSER_FIREFOX) || browser.equals("ff")) {
			driver = new FirefoxDriver();
		}
		else if (browser.contains(BROWSER_SAFARI)) {
			driver = new SafariDriver();
		}
		else if (browser.contains(BROWSER_IE) || browser.equals("ie")) {
			driver = new InternetExplorerDriver();
		}
		else {
			driver = new ChromeDriver();
		}
		
		return driver;	
	}

	public static WebDriver createWebDriver(String browser, String version, String platform, String hubUrl)
			throws MalformedURLException {

		// parse params
		browser = browser == null ? null : browser.toLowerCase();
		version = version == null ? null : version.toLowerCase();
		platform = platform == null ? null : platform.toLowerCase();

		DesiredCapabilities dc = createDesiredCapabilities(browser, version, platform);
		URL url = new URL(hubUrl);

		// setup driver
		WebDriver driver = new RemoteWebDriver(url, dc);

		return driver;
	}
}
