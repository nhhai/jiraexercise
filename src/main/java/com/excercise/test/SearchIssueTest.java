package com.excercise.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.excercise.common.Issue;
import com.excercise.page.DashboardPage;
import com.excercise.page.IssueDetailPage;
import com.excercise.page.LogInPage;
import com.excercise.page.component.CreateIssueForm;

public class SearchIssueTest extends JiraAutomationTest {

	@Test(dataProvider = "CreateIssueTestData")
	public void searchIssue(Issue issue) {

		// go to dashboard
		driver.navigate().to(homeUrl);
		DashboardPage dashboard = new DashboardPage(driver);

		// login
		LogInPage loginPage = dashboard.getMenuBar().clickLogIn();
		dashboard = loginPage.setEmail(email).setPassword(password).clickLogIn(DashboardPage.class);

		// click create
		CreateIssueForm createForm = dashboard.getMenuBar().clickCreate();

		// submit form
		createForm
			.setProject(issue.getProject())
			.setIssueType(issue.getIssueType())
			.setSummary(issue.getSummary())
			.setAssignee(issue.getAssignee())
			.createAndClose();

		// verify status and get issue id
		String message = dashboard.getPopupMessage().getMessageAtIndex(0);
		Assert.assertTrue(message.contains(issue.getSummary() + " has been successfully created."));
		String issueId = message.substring("Issue".length() + 1, message.indexOf(" - "));

		// search issue
		IssueDetailPage issuePage = dashboard.getMenuBar().quickSearchByText(issueId, IssueDetailPage.class);
		
		// verify page title
		String pageTitle = String.format("[%s] %s - Atlassian JIRA", issueId, issue.getSummary());
		Assert.assertEquals(issuePage.getPageTitle(), pageTitle, "Page title");
		
		// verify issue detail page
		Assert.assertEquals(issuePage.getIssueKey(), issueId, "Issue ID");
		Assert.assertEquals(issuePage.getIssueSummary(), issue.getSummary(), "Issue summary");
		Assert.assertEquals(issuePage.getIssueType(), issue.getIssueType(), "Issue type");
		Assert.assertEquals(issuePage.getIssueAssignee(), issue.getAssignee(), "Issue assignee");
	}
}
