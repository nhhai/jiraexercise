package com.excercise.test;

import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import com.excercise.aut.WebAutomationTest;
import com.excercise.common.Issue;

public class JiraAutomationTest extends WebAutomationTest {

	protected String homeUrl;
	protected String email;
	protected String password;
	
	@BeforeClass
	public void beforeClass(ITestContext ctx) {
		homeUrl = ctx.getSuite().getParameter("homeUrl");
		email = ctx.getSuite().getParameter("email");
		password = ctx.getSuite().getParameter("password");
	}
	
	@AfterClass
	public void afterClass() {
	}
	
	
	/*
	 * TEST DATA
	 * read from file or some other sources
	 * for the purpose of this exercise, I'll hardcode instead
	*/
	
    @DataProvider(name = "CreateIssueTestData", parallel = true)
    public Object[][] CreateIssueTestData() {
    	
    	Issue issue1 = new Issue();
    	
    	issue1.setProject("A Test Project");
    	issue1.setIssueType("Bug");
    	issue1.setSummary("Issue - User can create issue with empty summary");
    	issue1.setAssignee("Dummy User");
    	
        Object[][] data = { { issue1 } };

        return data;
    }
    
    @DataProvider(name = "UpdateIssueTestData", parallel = true)
    public Object[][] UpdateIssueTestData() {
    	
    	Issue issue1 = new Issue();
    	Issue issue1Updated = new Issue();
    	
    	issue1.setProject("A Test Project");
    	issue1.setIssueType("Bug");
    	issue1.setSummary("Update - User cannot update assignee");
    	issue1.setAssignee("Dummy User");
    	
    	issue1Updated.setSummary("Update - User cannot update summary");
    	
        Object[][] data = { { issue1, issue1Updated } };

        return data;
    }
}
