package com.excercise.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.excercise.common.Issue;
import com.excercise.page.DashboardPage;
import com.excercise.page.IssueDetailPage;
import com.excercise.page.LogInPage;
import com.excercise.page.component.CreateIssueForm;
import com.excercise.page.component.UpdateIssueForm;

public class UpdateIssueTest extends JiraAutomationTest {

	@Test(dataProvider = "UpdateIssueTestData")
	public void updateIssue(Issue issue, Issue updatedIssue) {

		// go to dashboard
		driver.navigate().to(homeUrl);
		DashboardPage dashboard = new DashboardPage(driver);

		// login
		LogInPage loginPage = dashboard.getMenuBar().clickLogIn();
		dashboard = loginPage.setEmail(email).setPassword(password).clickLogIn(DashboardPage.class);

		// click create
		CreateIssueForm createForm = dashboard.getMenuBar().clickCreate();

		// submit form
		createForm
			.setProject(issue.getProject())
			.setIssueType(issue.getIssueType())
			.setSummary(issue.getSummary())
			.setAssignee(issue.getAssignee())
			.createAndClose();

		// verify status and get issue id
		String message = dashboard.getPopupMessage().getMessageAtIndex(0);
		Assert.assertTrue(message.contains(issue.getSummary() + " has been successfully created."));
		String issueId = message.substring("Issue".length() + 1, message.indexOf(" - "));

		// search issue
		IssueDetailPage issuePage = dashboard.getMenuBar().quickSearchByText(issueId, IssueDetailPage.class);
		
		// verify page title and key id
		String pageTitle = String.format("[%s] %s - Atlassian JIRA", issueId, issue.getSummary());
		Assert.assertEquals(issuePage.getPageTitle(), pageTitle, "Page title");
		Assert.assertEquals(issuePage.getIssueKey(), issueId, "Issue ID");
		
		// edit issue
		UpdateIssueForm updateForm = issuePage.clickEdit();
		if (updatedIssue.getSummary() != null) {
			updateForm.setSummary(updatedIssue.getSummary());
		} 
		if (updatedIssue.getIssueType() != null) {
			updateForm.setIssueType(updatedIssue.getIssueType());
		} 
		if (updatedIssue.getAssignee() != null) {
			updateForm.setAssignee(updatedIssue.getAssignee());
		}
		issuePage = updateForm.clickUpdate(IssueDetailPage.class).delay(3);
		
		// verify page title and issue detail is updated
		if (updatedIssue.getSummary() != null) {
			pageTitle = String.format("[%s] %s - Atlassian JIRA", issueId, updatedIssue.getSummary());
			Assert.assertEquals(issuePage.getPageTitle(), pageTitle, "Page title");
			Assert.assertEquals(issuePage.getIssueSummary(), updatedIssue.getSummary(), "Updated summary");
		} 
		if (updatedIssue.getIssueType() != null) {
			Assert.assertEquals(issuePage.getIssueType(), updatedIssue.getIssueType(), "Updated issue type");
		} 
		if (updatedIssue.getAssignee() != null) {
			Assert.assertEquals(issuePage.getIssueAssignee(), updatedIssue.getAssignee(), "Updated assignee");
		}
	}
}
