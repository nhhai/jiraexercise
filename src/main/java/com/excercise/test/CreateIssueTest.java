package com.excercise.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.excercise.common.Issue;
import com.excercise.page.DashboardPage;
import com.excercise.page.LogInPage;
import com.excercise.page.component.CreateIssueForm;

public class CreateIssueTest extends JiraAutomationTest {
	
	@Test(dataProvider = "CreateIssueTestData")
	public void createIssue(Issue issue) {
		
		// go to dashboard
		driver.navigate().to(homeUrl);
		DashboardPage dashboard = new DashboardPage(driver);
		
		// login
		LogInPage loginPage = dashboard.getMenuBar().clickLogIn();
		dashboard = loginPage
			.setEmail(email)
			.setPassword(password)
			.clickLogIn(DashboardPage.class);
		
		// click create
		CreateIssueForm createForm = dashboard.getMenuBar().clickCreate();
		
		// submit form
		createForm
			.setProject(issue.getProject())
			.setIssueType(issue.getIssueType())
			.setSummary(issue.getSummary())
			.setAssignee(issue.getAssignee())
			.createAndClose();
					
		// verify status
		String message = dashboard.getPopupMessage().getMessageAtIndex(0);
		dashboard.delay(5);
		Assert.assertTrue(message.contains(issue.getSummary() + " has been successfully created."));
	}
}
