#Project
Since the exercise is not so clear, I don't know if I need to support cross-browsers, grid... or not. Therefore, I wrote the automation tests which supports the following:
- Cross-browsers: Chrome, Firefox, IE, Safari (not tested)
- Selenium Grid: supports above browsers on Windows, Mac, Linux. Supported browser versions depend on the version of selenium-server
- Data driven
- Run from command line


#Requisite:
- Maven
- Eclipse + TestNG plugin + Maven plugin (optional)


#Running:
- To run from command line:
```
cd /to/my/project
mvn clean install
cd target/output
java -cp atlassian-test.jar org.testng.TestNG testng.xml
```
- To run from Eclipse
  + Import maven project
  + Right click on testng.xml and Run As > TestNG suite (require TestNG plugin)


#Cross-browsers note:
- Edit file test.properties: make sure paths to your drivers and binary are correct
- To be able to run on IE:
  + Go to Internet Options >  Security, uncheck Enable Protected Mode for all zones
  + Set security level of Internet zone to Medium
- You can edit testng.xml file and change the value follow the instruction in that file to make the test run on other browsers. You can also add new <test> tags to run on multiple browsers at once.


#Selenium Grid note:
- In order to the test with selenium grid, you must set up a grid first (https://code.google.com/p/selenium/wiki/Grid2)
- Change the config in testng.xml follow the instructions


#Data driven note:
- For the purpose of this exercise, I only provide a concept to do data driven testing and my explaination is put in com.exercise.test.JiraAutomationTest file
- To add / update the data, open JiraAutomationTest.java file and add some codes yourself ^^